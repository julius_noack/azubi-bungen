/**
 * Created by julius on 27.06.16.
 */
var uniqueID = (function() {
    var id = 0;
    return function() { return id++; };  // Return and increment
})();

(function ( $ ) {
    $.fn.sdLoader = function(blOn) {
        var position = this.position();
        if(blOn) {
            console.log("injectDaShit");
            if (injectDaShit(this)) {
                $('#injected').css({
                    "left": position.left + "px",
                    "top": position.top + "px",
                    "width": this.width() + "px",
                    "height": this.height() + "px"
                }).show();
                $('#injected > div').css("top", ((this.height() / 2) - (65 / 2)) - 3.14 + "px");
                $('#injected').removeAttr('id');
            }
        } else {
            var myEvent = $.Event('endLoading',{bubbles:true});
            $(this).trigger(myEvent);
        }
        return this;
    };

    function injectDaShit(node){
        var id = uniqueID();
        var dataSet = $(node).data();
        if (dataSet.elementId !== undefined && dataSet.elementId !== null) {
            return false;
        }
        //injecting the loader
        $('body').append('<div id="injected" class="loader-holder"><div class="loader"></div></div>');

        //setting the ids
        $('#injected').data("elementId", id);
        $(node).data("elementId", id);

        //when the event bubbles up check if it is for the element
        $('body').on('endLoading',function(e){
            var elements = $('.loader-holder');
            for(var i=0;i < elements.length;i++) {
                if ($(e.target).data("elementId") == $(elements[i]).data("elementId")){
                    $(elements[i]).remove();
                    $(e.target).data("elementId",null);
                    break;
                }
            }
            e.stopPropagation();
        });
        return true;
    }

}( jQuery ));