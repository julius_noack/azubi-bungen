<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 05.09.16
 * Time: 11:00
 */

$sHosts = shell_exec("timeout 5  java -jar ./lib/jmdns.jar -bs _airplay._tcp local.");
$aHosts = '';

preg_match_all("/name: '(.+?)\\.\\_airplay/", $sHosts, $aHosts);
$aFilteredHosts = array_unique($aHosts[1]);

echo PHP_EOL;

foreach ($aFilteredHosts as $sIndex => $sHostname) {
    echo ($sIndex + 1) . '. ' . $sHostname . PHP_EOL;
}

echo PHP_EOL;

$iHostNumber = (readline('Welches Geraet soll benutzt werden?') - 1);

echo PHP_EOL;

echo 'Es wuerd auf ' . $aHosts[1][$iHostNumber] . '.local gestreamt...';

shell_exec('java -jar ./lib/airplay.jar -h ' . $aHosts[1][$iHostNumber] . '.local -d');