<?php

namespace ukp\Output;

use ukp\Interfaces\Output as iOutput;

class Console implements iOutput
{
    const GREEN = "[42m";
    const RED = "[41m";
    const BLUE = "[44m";
    const YELLOW = "[43m";
    const NORMAL = "[0m";
    const BLA = "asd";

    public function out($sString, $sColor = null){
        try{
            if ($sColor === null) {
                echo $sString;
            } else {
                echo chr(27) . $sColor . $sString . chr(27) .self::NORMAL;
            }
        }catch(\Exception $e){
            return false;
        }
        return true;
    }
}
