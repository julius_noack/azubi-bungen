<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 01.07.16
 * Time: 11:12
 */

include_once __DIR__."/../../vendor/autoload.php";

$oAnalytics = new \ukp\Analytics\Analyser();

while (true) {
    $handle = fopen ("php://stdin","r");
    $line = fgets($handle);
    $oAnalytics->analyse($line);
}
fclose($handle);