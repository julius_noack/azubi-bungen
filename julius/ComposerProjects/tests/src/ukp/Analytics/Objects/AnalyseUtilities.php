<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 05.07.16
 * Time: 16:49
 */

namespace ukp\Analytics\Objects;
use ukp\Analytics\Objects\Satz;
use ukp\Analytics\Objects\MiniDB;

class AnalyseUtilities
{
    static function isArtikelInSatz(Satz $oSatz){
        foreach ($oSatz->getWords() as $oWord) {
            if (array_search(strtolower($oWord->getText()), MiniDB::$_sArtikel)) {
                return true;
            }
        }
        return false;
    }

    static function getWordAfterSein(Satz $oSatz)
    {
        $blFound = false;
        foreach ($oSatz->getWords() as $oWord){
            if ($blFound) {
                return $oWord;
            }
            if (array_search(strtolower($oWord->getText()), MiniDB::$_sSein["gegenwart"])){
                $blFound = true;
            }
        }
        return false;
    }

    static function isUppercase($sWord)
    {
        if(ucfirst($sWord) === $sWord){
            return true;
        }
        return false;
    }
}