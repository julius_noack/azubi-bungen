<?php

namespace ukp\Analytics\Objects;

class Satz
{
    const LAST_WORD = 1;
    const FIRST_WORD = 2;

    const AUSSAGE_SATZ = 1;
    const FRAGE_SATZ = 2;
    const BEFEHLS_SATZ = 4;
    const AUSRUFE_SATZ = 8;

    protected $_iType;
    protected $_aWords = [];
    
    public function __construct(array $aWords)
    {
        $this->_aWords = $aWords;
    }

    public function getWords()
    {
        return $this->_aWords;
    }

    public function getRawText()
    {
        $aArray = [];
        foreach ($this->_aWords as $oWord) {
            $aArray[] = $oWord->getText();
        }
        return $aArray;
    }

    public function setWords(array $aWords)
    {
        $this->_aWords = $aWords;
    }

    public function setType($iType)
    {
        $this->_iType = $iType;
    }

    public function getType()
    {
        return $this->_iType;
    }

    public function getWordsCount()
    {
        return count($this->_aWords);
    }

    public function getWord($iPos, $iFlag = null)
    {
        $sWord = null;
        if($iFlag == null){
            $sWord = $this->_aWords[$iPos];
        } else {
            $LastWord = ($iFlag & self::LAST_WORD) == self::LAST_WORD;
            $FirstWord = ($iFlag & self::FIRST_WORD) == self::FIRST_WORD;
            if ($LastWord) {
                $sWord = $this->_aWords[count($this->_aWords)-1];
            }
            if ($FirstWord) {
                $sWord = $this->_aWords[0];
            }
        }
        return $sWord;
    }

    public function setWord($sWord, $iPos, $iFlag = null)
    {
        if($iFlag == null){
            $this->_aWords[$iPos] = $sWord;
        } else {
            $LastWord = ($iFlag & self::LAST_WORD) == self::LAST_WORD;
            $FirstWord = ($iFlag & self::FIRST_WORD) == self::FIRST_WORD;
            if ($LastWord) {
                $this->_aWords[count($this->_aWords)-1] = $sWord;
            }
            if ($FirstWord) {
                $this->_aWords[0] = $sWord;
            }
        }
    }
}
