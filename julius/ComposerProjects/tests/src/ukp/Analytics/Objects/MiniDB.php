<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 04.07.16
 * Time: 09:28
 */

namespace ukp\Analytics\Objects;


class MiniDB
{
    static $_sArtikel = ['der', 'die', 'das'];
    static $_sPronomen = ['ich', 'du', 'er', 'sie', 'es', 'wir', 'ihr', 'sie'];
    static $_sFrageWoerter = ['wer', 'was', 'wie', 'warum', 'wieso', 'weshalb'];
    static $_sSein = [
        "gegenwart" => ['bin','bist','ist','seid','sind'],
        "zukunft" => ['werde' ,'werdest', 'werden' , 'werdet' ,'wirst'],
        "vergangenheit" => ['waren', 'wart', 'warst', 'war', 'wäre', 'hat', 'hatte']
    ];
}