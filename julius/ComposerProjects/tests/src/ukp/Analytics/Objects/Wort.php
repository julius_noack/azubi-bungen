<?php

namespace ukp\Analytics\Objects;

class Wort
{
    const SUBSTANTIV = 1;
    const ADJECTIV = 2;
    const VERB = 4;
    const ARTIKEL = 8;
    const PARTIZIP = 16;
    const ADVERB = 32;
    const NUMERAL = 64;

    const SUBJECT = 128;
    const PRAEDIKAT = 256;
    const ATTRIBUTE = 512;
    const OBJECT = 1024;


    protected $_sWort;
    protected $_iPos;
    protected $_iType;

    public function __construct($sWort, $iPos = null)
    {
        $this->_sWort = $sWort;
        if($iPos !== null){
            $this->setPosition($iPos);
        }
    }

    public function getText()
    {
        return $this->_sWort;
    }

    public function setPosition($iPos)
    {
        $this->_iPos = $iPos;
    }

    public function getPosition()
    {
        return $this->_iPos;
    }

    public function getType()
    {
        return $this->_iType;
    }

    public function  setType($iType)
    {
        $this->_iType = $iType;
    }
}
