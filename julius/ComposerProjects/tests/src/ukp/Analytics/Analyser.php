<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 05.07.16
 * Time: 16:32
 */

namespace ukp\Analytics;

use ukp\Analytics\Objects\MiniDB;
use ukp\Analytics\Objects\Wort;
use ukp\Analytics\Objects\Satz;
use ukp\Analytics\Analysers\SatzTypeAnalyser;
use ukp\Analytics\Analysers\WordTypesAnalyser;

class Analyser
{
    public function analyse($sSatz)
    {
        $oSatz = $this->_createSatzFromString($sSatz);
        $oSatzTypeAnalyser = new SatzTypeAnalyser();
        $oSatz->setType($oSatzTypeAnalyser->analyseSatz($oSatz));
        $oWordTypeAnalyser = new WordTypesAnalyser();
        $oSatz = $oWordTypeAnalyser->analyseWordTypes($oSatz);
        var_dump($oSatz);
    }

    protected function _createSatzFromString($sSatz)
    {
        $aReturn = [];
        preg_match_all("/(der [^\\s]+)|(die [^\\s]+)|(das [^\\s]+)|([^\\s]+)/i", $sSatz, $aWords);
        foreach ($aWords[0] as $iPosition => $sWord) {
            $aReturn[] = new Wort($sWord,$iPosition);
        }
        return new Satz($aReturn);
    }
}