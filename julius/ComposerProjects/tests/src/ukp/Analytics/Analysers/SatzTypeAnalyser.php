<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 05.07.16
 * Time: 16:37
 */

namespace ukp\Analytics\Analysers;
use ukp\Analytics\Objects\MiniDB;
use ukp\Analytics\Objects\Satz;

class SatzTypeAnalyser
{
    static function analyseSatz(Satz $oSatz)
    {
        $iType = null;
        $aRawText = $oSatz->getRawText();
        foreach ($aRawText as $sWord) {
            if (in_array(strtolower($sWord), MiniDB::$_sFrageWoerter)) {
                echo "Fragesatz\n";
                return Satz::FRAGE_SATZ;
            }
        }

        if (preg_match("/(e)$/i", $oSatz->getWord(0,Satz::FIRST_WORD)->getText()) && !array_search(strtolower($oSatz->getWord(0,Satz::FIRST_WORD)->getText()), MiniDB::$_sArtikel)) {
            echo "Befehlssatz\n";
            return Satz::BEFEHLS_SATZ;
        } else {
            echo "Aussagesatz\n";
            return Satz::AUSSAGE_SATZ;
        }
    }
}