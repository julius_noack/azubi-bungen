<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 05.07.16
 * Time: 16:41
 */

namespace ukp\Analytics\Analysers;
use ukp\Analytics\Objects\Satz;
use ukp\Analytics\Objects\Wort;
use ukp\Analytics\Analysers\WordAnalysers;

class WordTypesAnalyser
{
    public function analyseWordTypes(Satz $oSatz)
    {
        $iWordType = Wort::PRAEDIKAT | Wort::SUBJECT;
        $oWord = false;
        $blAll = false;
        $iType = 0;
        While (!$blAll) {
            switch (true){
                case (($iWordType & Wort::SUBJECT) == Wort::SUBJECT):
                    $oWord = WordAnalysers\SubjektAnalyser::getSubject($oSatz);
                    $iWordType = $iWordType & ~Wort::SUBJECT;
                    $iType = Wort::SUBJECT;
                    break;
                case (($iWordType & Wort::PRAEDIKAT) == Wort::PRAEDIKAT):
                    $oWord = WordAnalysers\PreadikatAnalyser::getPreadikat($oSatz);
                    $iWordType = $iWordType & ~Wort::PRAEDIKAT;
                    $iType = Wort::PRAEDIKAT;
                    break;
                case (($iWordType & Wort::OBJECT) == Wort::OBJECT):
                    $oWord = WordAnalysers\ObjectAnalyser::getObject($oSatz);
                    $iWordType = $iWordType & ~Wort::OBJECT;
                    $iType = Wort::OBJECT;
                    break;
                case (($iWordType & Wort::ADVERB) == Wort::ADVERB):
                    $iWordType = $iWordType & ~Wort::ADVERB;
                    $iType = Wort::ADVERB;
                    break;
                default:
                    $blAll = true;
            }
        }

        if ($oWord) {
            $oWord->setType($iType);
            $oSatz->setWord($oWord, $oWord->getPosition());
        }

        return $oSatz;
    }
}