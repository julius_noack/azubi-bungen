<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 05.07.16
 * Time: 16:39
 */

namespace ukp\Analytics\Analysers\WordAnalysers;
use ukp\Analytics\Objects\Satz;

class PreadikatAnalyser
{
    static function getPreadikat(Satz $oSatz)
    {
        $oWord = false;
        if (strtolower($oSatz->getWord(0, Satz::FIRST_WORD)->getText()) === 'wie') {
            $oWord = $oSatz->getWord(1);
        } else if(strtolower($oSatz->getWord(0, Satz::FIRST_WORD)->getText()) === 'was' || strtolower($oSatz->getWord(0, Satz::FIRST_WORD)->getText()) === 'wer') {
            $oWord = $oSatz->getWord(0, Satz::LAST_WORD);
        }
        return $oWord;
    }
}