<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 05.07.16
 * Time: 16:39
 */

namespace ukp\Analytics\Analysers\WordAnalysers;
use ukp\Analytics\Objects\Satz;
use ukp\Analytics\Objects\AnalyseUtilities;

class SubjektAnalyser
{
    static function getSubject(Satz $oSatz)
    {
        $oWord = false;
        if (strtolower($oSatz->getWord(0, Satz::FIRST_WORD)->getText()) !== 'wer') {
            if (AnalyseUtilities::isArtikelInSatz($oSatz)) {
                $oWord = AnalyseUtilities::getWordAfterSein($oSatz);
            }
        }
        return $oWord;
    }
}