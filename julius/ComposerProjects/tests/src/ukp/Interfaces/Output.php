<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 01.07.16
 * Time: 09:22
 */

namespace ukp\Interfaces;


interface Output
{
    public function out($sString, $sColor = null);
}