<?php

namespace spec\ukp\Analytics;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class TextSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('ukp\Analytics\Text');
    }

    function it_is_analysing()
    {
        $this->analyse("Hallo, wie geht es dir?")->shouldReturnAnInstanceOf('ukp\Analytics\Objects\Satz');
    }
}
