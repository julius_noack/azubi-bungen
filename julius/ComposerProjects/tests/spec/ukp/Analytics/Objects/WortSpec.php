<?php

namespace spec\ukp\Analytics\Objects;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class WortSpec extends ObjectBehavior
{
    function let(){
        $this->beConstructedWith("Baum");
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('ukp\Analytics\Objects\Wort');
    }

    function it_should_Return_its_Text()
    {
        $this->getText()->shouldReturn("Baum");
    }

    function it_should_have_a_type()
    {
        $this->getType()->shouldReturn("Substantiv");
    }

    function it_should_have_position()
    {
        $this->setPosition(5);
        $this->getPosition()->shouldReturn(5);
    }
}
