<?php

namespace spec\ukp\Analytics\Objects;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SatzSpec extends ObjectBehavior
{
    function let(){
        $this->beConstructedWith(["hallo","wie","geht","es","dir"]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('ukp\Analytics\Objects\Satz');
    }

    function it_should_give_me_its_words()
    {
        $this->getWords()->shouldReturn(["hallo","wie","geht","es","dir"]);
    }

    function it_should_be_able_to_set_words()
    {
        $this->setWords(["hallo","wie","geht","es","dir","heute"]);
        $this->getWords()->shouldReturn(["hallo","wie","geht","es","dir","heute"]);
    }

    function it_should_return_count_of_words()
    {
        $this->getWordsCount()->shouldReturn(5);
    }

    function it_should_return_a_words()
    {
        $this->getWord(0, \ukp\Analytics\Objects\Satz::LAST_WORD)->shouldReturn("dir");
    }
}
