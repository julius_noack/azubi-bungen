<?php

namespace spec\ukp\Output;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ConsoleSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('ukp\Output\Console');
        $this->shouldImplement('ukp\Interfaces\Output');
    }

    function it_is_outputting()
    {
        $this->out("LOL")->shouldReturn(true);
    }

    function it_is_coloring()
    {
        $this->out("LOL", \ukp\Output\Console::GREEN)->shouldReturn(true);
    }
}
