<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 24.08.16
 * Time: 14:57
 */


include __DIR__ .'/../vendor/autoload.php';

use Symfony\Component\Console\Application;

$oApplication = new Application();

$oApplication->add(new \UrlCrawler\Crawler());

$oApplication->run();