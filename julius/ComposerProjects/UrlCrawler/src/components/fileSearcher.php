<?php

namespace UrlCrawler\components;

/**
 * Class fileSearcher
 * @package UrlCrawler\components
 */
class fileSearcher
{
    protected $_oFinder = null;

    /**
     * fileSearcher constructor.
     * @param object $oFinder
     */
    public function __construct($oFinder)
    {
        $this->_oFinder = $oFinder;
    }

    /**
     * @param string $sDir
     * @param string $sPattern
     * @param string $sContains
     * @return array
     */
    public function searchFiles($sDir, $sPattern, $sContains = null)
    {
        $aResults = [];
        $oFinder = $this->_oFinder;
        if ($sContains === null) {
            $oFinder->files()->in($sDir)->name($sPattern);
        } else {
            $oFinder->files()->in($sDir)->name($sPattern)->contains($sContains);
        }
        foreach ($oFinder as $oFile) {
            $aResults[] = $oFile;
        }
        return $aResults;
    }
}
