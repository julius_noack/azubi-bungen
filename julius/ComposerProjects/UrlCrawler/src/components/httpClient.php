<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 24.08.16
 * Time: 14:58
 */

namespace UrlCrawler\components;

/**
 * Class httpClient
 * @package UrlCrawler\components
 */
class httpClient
{
    /**
     * @param string $sUrl
     * @return bool
     */
    public function request($sUrl)
    {
        $aResult = @get_headers($sUrl);
        if (strpos($aResult[0], '200 OK')) {
            return true;
        }
        return false;
    }
}