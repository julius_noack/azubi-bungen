<?php

namespace UrlCrawler\components;

/**
 * Class urlChecker
 * @package UrlCrawler\components
 */
class urlChecker
{
    protected $_oHttpClient = null;

    /**
     * urlChecker constructor.
     * @param object $oHttpClient
     */
    public function __construct($oHttpClient)
    {
        $this->_oHttpClient = $oHttpClient;
    }

    /**
     * @param array $aUrls
     * @return array
     */
    public function checkUrls(array $aUrls)
    {
        $aReachableUrl = [];
        foreach ($aUrls as $sUrl) {
            $blReachable = $this->_oHttpClient->request($sUrl);
            if ($blReachable) {
                $aReachableUrl['yes'][] = $sUrl;
            } else {
                $aReachableUrl['no'][]  = $sUrl;
            }
        }
        return $aReachableUrl;
    }
}
