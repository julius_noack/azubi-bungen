<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 30.08.16
 * Time: 09:58
 */

namespace UrlCrawler;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use \UrlCrawler\components\fileSearcher;
use \UrlCrawler\components\urlChecker;
use \UrlCrawler\components\httpClient;

/**
 * Class Crawler
 * @package UrlCrawler
 */
class Crawler extends Command
{
    /**
     * Standart Symfony
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('crawler:run')

            // the short description shown while running "php bin/console list"
            ->setDescription('Crawls a Online Service.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("The application Crawls all config file and searchs on remote for rechable config files.");

        $this
            ->addArgument('dir', InputArgument::REQUIRED, 'the Folder to scan for config files.')
            ->addArgument('url', InputArgument::REQUIRED, 'the url to scan for readable config files')
            ->addArgument('regex', InputArgument::OPTIONAL, 'the regex witch matches the name of the config files')
            ->addOption('contains', 'c', InputOption::VALUE_OPTIONAL, 'The Text that should be in the config file(regex possible)')
            ->addOption('progress', 'p', InputOption::VALUE_NONE, 'Shows the Checked Files.')
        ;
    }

    /**
     * @param InputInterface  $oInput
     * @param OutputInterface $oOutput
     */
    protected function execute(InputInterface $oInput, OutputInterface $oOutput)
    {
        //@TODO auslagern,tests
        $oUrlChecker = new urlChecker(new httpClient());
        $oFileSearcher = new fileSearcher(\Symfony\Component\Finder\Finder::create());
        
        $sDir = $oInput->getArgument('dir');
        $sUrl = $oInput->getArgument('url');
        $sRegex = $oInput->getArgument('regex');
        $sContains = $oInput->getOption('contains');
        $blProgress = $oInput->getOption('progress');
        $aCheckUrls = [];

        $aListOfFiles = $oFileSearcher->searchFiles($sDir, $sRegex, $sContains);

        foreach ($aListOfFiles as $oFile) {
            $aCheckUrls[] = $sUrl . str_replace($sDir, '', $oFile->getPathname());
        }

        $aResults = $oUrlChecker->checkUrls($aCheckUrls, $oOutput);

        $oOutput->write(var_dump($aResults));

    }

}