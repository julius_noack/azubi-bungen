<?php

namespace spec\UrlCrawler\components;

use UrlCrawler\components\httpClient;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class httpClientSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(httpClient::class);
    }

    function it_can_make_a_request()
    {
        $this->request('http:///')->shouldReturn(false);
    }
}
