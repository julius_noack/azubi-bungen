<?php

namespace spec\UrlCrawler\components;

use UrlCrawler\components\urlChecker;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class urlCheckerSpec extends ObjectBehavior
{
    function let(\UrlCrawler\components\httpClient $oHttpClient)
    {
        $this->beConstructedWith($oHttpClient);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(urlChecker::class);
    }

    function it_should_check_urls(\UrlCrawler\components\httpClient $oHttpClient)
    {
        $oHttpClient->request('http://google.de')->shouldBeCalled()->willReturn(true);
        $this->checkUrls(['http://google.de']);
    }
}
