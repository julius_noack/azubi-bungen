<?php

namespace spec\UrlCrawler\components;

use UrlCrawler\components\fileSearcher;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class fileSearcherSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(\Symfony\Component\Finder\Finder::create());
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(fileSearcher::class);
    }

    function it_could_search()
    {
        $this->searchFiles('./','')->shouldReturn([]);
    }
}
