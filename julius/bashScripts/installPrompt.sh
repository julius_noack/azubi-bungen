#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

package[0]="codeception/codeception:*"
package[1]="phpspec/phpspec:*"

function checkDepedency() {
    command_name=$1
    if ! php_loc="$(type -p "$command_name")" || [ -z "$php_loc" ]; then
            eval "$2=false"
        else
            eval "$2=true"
    fi
}

function installDependencies(){

        php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
        php -r "if (hash_file('SHA384', 'composer-setup.php') === 'e115a8dc7871f15d853148a7fbac7da27d6c0030b848d9b3dc09e2a0388afed865e6a3d6b3c0fad45c48e2b5fc1196ae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
        php composer-setup.php
        php -r "unlink('composer-setup.php');"

        echo "including dependencies..."
        for dependency in "${package[@]}"
        do
            php composer.phar require $dependency
        done
        php composer.phar install
        echo "done"
}

function BuildModule() {
    echo "checking Dependencies for Dependency-Manager"
    isPhpInstalled=''
    checkDepedency "php" isPhpInstalled
    if [ $isPhpInstalled = "true" ];then
            echo "php is installed"
            installDependencies
        else
            echo "php is not installed"
            exit
    fi
}

echo "Do you wish to install the "
select yn in "Yes" "No"; do
    case $yn in
        Yes ) BuildModule; break;;
        No ) exit;;
    esac
done